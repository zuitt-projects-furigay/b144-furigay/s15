// Operators
/*
	Add (+) - sum
	Subtract (-) - Difference
	Divide (/) - Quotient
	Modulos (%) - Remiander

*/

function mod() {
	return 9 % 2
} 

console.log(mod());

// PEMDAS
// 8 * (6 + 2) - (2 * 3)


// Assignmment Operator (=)
/*
	+= (Addition)
	-= (Subtraction)
	*= (Multiplication)
	/= (Division)
	%= (Modulo)
*/

	let x= 1;

	let sum = 1;
	// sum = sum + 1;

	sum += 1;
 

	console.log(sum);

// Increment (++) and Decrement (--)
/*
	Operators that add or subtract values by 1 and reasigns the values of the variable where the increment/decrement was applied to.
*/


let z = 1;

// Pre-Increment
// add first
let increment = ++z;
console.log('Result of pre-increment: ' + increment);
console.log('Result of pre-increment: ' + z);

// Post-Increment

increment = z++;
console.log("Result of post-Increment: " + increment);
console.log("Result of post-Increment: " + z);


// Pre-Declement
let decrement = --z;
console.log("Result of pre-Increment: " + decrement);
console.log("Result of pre-Increment: " + z);

// Post-Increment
decrement = z--;
console.log("Result of post-Increment: " + decrement);
console.log("Result of post-Increment: " + z);


// Comparison Operators

// Equality Operator - (==) is eqaul to.
let juan = 'juan';

console.log(1 == 1);
console.log(0 == false);
console.log('juan' == juan)

// Strick Eqaulity - (===)
console.log(1 === true);

// Inequality Operator (!=) - not equal to.
console.log("Inequality Operator");
console.log(1 != 1);
console.log('Juan' != juan);

// Strick Inequality (!==)
console.log(0 !== false);


// Other comparison Operators
/*
	> - greater than
	< - less than
	>= - Greater than or equal
	<= - less than or equal

*/

// Logical Operators

let isLegalAge  = true;
let isRegistered = false;
/*
	And Operator (&&) - Returns true if all operannds are true
	EX.
		true && true = true
		false && true = false
		true && false = false
		false && false = false
	
	Or Operator (||) - Returns true at least on operands is true.
	EX. 
		true && true = true
		false && true = true
		true && false = true
		false && false = false
*/

// AND (&&) Operator
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// OR (||) Operator
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Selection Control Structres
/*
	If statement
		-execute a statement if a specific condition is true

	Syntax 

	if(condition){
		statement/s;
	}
*/ 

let num = -1;

if (num < 0) {
	console.log('Hello');
}


let value = 10;

if (value >= 10) {
	console.log('Welcome to Zuitt')
}

/*
	If-else Statement
		-execute a statement if the previous condition returns false.
	Syntax:
	if(condition) {
		statement/s
	}
	else {
		statement/s
	}
*/

num1 = 5
if (num1 >= 10) {
	console.log("Number is greater or equal to 10")
}
else{
	console.log("Number is not greater or equal to 10" )
}


// Mini Activity
/*
	Create a condition that will return the stateemnt "Senior age" if the age is greater than 59, Otherwise display the statement "Invalid age".
*/

// to convert string to integer, use parseInt()
// let age = parseInt(prompt("Please provide age: "));

// if (age > 59) {
// 	alert("Senior Age")
// }
// else{
// 	alert("Invalid Age")
// }





// IF-ELSEIF-ELSE Statement

/*
	Syntax
	if(){
		statement/s;
	}
	else if(condition) {
		statement/s;
	}
	else if(condition) {
		statement/s;
	}
	.
	.
	.
	else {
	}


	1 - Quezon City
	2 - Valenzuela City
	3 - Pasig City
	4 - Taguig 
*/

// let city = parseInt(prompt("Enter a number: "));

// if (city === 1) {
// 	alert("Welcome to Quezon City");
// }
// else if(city ===2) {
// 	alert("Welcome to Valenzuela City");
// }
// else if(city ==3) {
// 	alert("Welcome to Pasig City");
// }
// else if(city ===4) {
// 	alert("Welcome to Taguig City");
// }
// else {
// 	alert("Invalid Number");
// }


let message = '';

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed < 30) {
		return 'Not a typhoon yet.';
	}
	else if(windSpeed <= 61) {
		return 'Tropical depression detected.';
	}
	else if(windSpeed >= 61 && windSpeed <=88) {
		return 'Tropical storm detected';
	}
	else if(windSpeed >=899 && windSpeed <=117) {
		return 'Severe tropical storm detected.';
	}
	else {
		return 'Typhoon detected.';
	}
}

message = determineTyphoonIntensity(70);
console.log(message);






// Ternary Operator
/*
	Syntax:
		(condition) ? ifTrue : ifFalse

*/

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);





let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under legal age limit';
}

let age = parseInt(prompt("What is your age?"));
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
alert("Result of Ternary Operator in Function: " + legalAge + ", " + name);






// Switch Statement
/*
	Syntax:
		switch (expression) {
			case value:
				statement/s;
				break;
			case value2 
				statement/s;
				break;
			case valuen:
				statement/s;
				break;
			default:
				statement/s;
		}
*/


let day = prompt("What day of the week is it today?").toLowerCase();

switch (day){
	case 'sunday':
		alert("The color of the day is red");
		break;
	case 'monday':
		alert("The color of the day is orange");
		break;
	case 'tuesday':
		alert("The color of the day is yellow");
		break;
	case 'wednesday':
		alert("The color of the day is green");
		break;
	case 'thursday':
		alert("The color of the day is blue");
		break;
	case 'friday':
		alert("The color of the day is indigo");
		break;
	case 'saturday':
		alert("The color of the day is violet");
		break;	
	default:
		alert("Please input valid day")		

}
 

/*
	TRY-CATCH-FINALLY Statement
		- Commonly used foor error handling.

*/

function showIntensityAlert(windSpeed) {
	try{
		alertat(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log(typeof error);
		console.warn(error.message);
	}
	finally {
		alert('Intensity updates will show new alert.')
	}
}

showIntensityAlert(56);


/*
	SYNTAX:



*/